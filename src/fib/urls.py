from django.urls import path

from . import views

app_name = 'fib'
urlpatterns = [
    # /fib/fibAPI?n=20
    path('fibAPI', views.fibAPI, name='fibAPI'),

    # /fib/xhr
    path('xhr', views.xhr, name='xhr'),

    # /fib/fetch
    path('fetch', views.fetch, name='fetch'),

    # /fib/
    path('', views.index, name='index'),
]
