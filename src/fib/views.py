from django.shortcuts import render
from django.http import JsonResponse


def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n-1) + fibonacci(n-2)


def fibAPI(request):
    # loop through the GET params and print them out
    for key in request.GET:
        print(f"\t{key} => {request.GET[key]}")

    if 'n' in request.GET:
        n = request.GET['n']
        if n.isnumeric():
            n = int(n)
            if n < 40:
                fib = fibonacci(int(n))
                response = {
                        'n': request.GET['n'],
                        'fibonacci': fib,
                        }
            else:
                response = {
                    'error': "Your 'n' parameter is too large",
                }

        else:
            response = {
                    'error': "Your 'n' parameter wasn't numeric :(",
                }
    else:
        response = {
                'error': 'You must supply a parameter "n"',
                }
    return JsonResponse(response)



def index(request):
    return render(request, 'fib/index.html')


def xhr(request):
    return render(request, 'fib/xhr.html')


def fetch(request):
    return render(request, 'fib/fetch.html')
