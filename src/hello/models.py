from django.db import models

class HighFiveCount(models.Model):
    slaps = models.IntegerField(default=0)


class HighFiveLog(models.Model):
    when = models.DateTimeField()
    who  = models.CharField(max_length=128)

