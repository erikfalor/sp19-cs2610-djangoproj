from django.urls import path

from . import views

urlpatterns = [
    path('highFive', views.highFive, name='highFive'),
    path('', views.index, name='index'),
]
