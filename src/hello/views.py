from django.shortcuts import render
from django.http import HttpResponse
from time import strftime
from django.utils import timezone

from .models import HighFiveLog

def index(request):
    numbers = [7, 42, 1337]

    highFiveCount = HighFiveLog.objects.count()
    return render(request, 'hello/index.html',
        {
            'highFiveCount': highFiveCount,
            'now': strftime('%c'),
            'numbers': numbers,
        })


def highFive(request):
    log = HighFiveLog(when=timezone.now(), who = request.META['REMOTE_ADDR'])
    log.save()

    highfives = HighFiveLog.objects.all()
    return render(request, 'hello/highFive.html', { 'highFives': highfives })

