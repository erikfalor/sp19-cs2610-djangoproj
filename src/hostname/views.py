from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return render(request, 'hostname/index.html', { 'host':  request.get_host() })
